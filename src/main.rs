#![feature(once_cell)]

mod config;
mod openweather;

use std::time::Duration;

use crate::{config::Config, openweather::OpenWeather};
use chrono::prelude::*;
use nannou::prelude::*;

fn main() {
    if let Err(err) = Config::get() {
        println!("Could not load config: {}", err);
        std::process::exit(1);
    }
    nannou::app(app_init).event(on_event).view(view).run();
}

struct Model {
    time: DateTime<Local>,
    weather: Result<OpenWeather, String>,
}

fn app_init(app: &App) -> Model {
    let window_id = app
        .new_window()
        .size(1280, 720)
        .visible(false)
        .build()
        .unwrap();
    let window = app.window(window_id).unwrap();

    let available_monitors = app.available_monitors();
    if available_monitors.len() > 1 {
        let primary = app.primary_monitor();
        let target_monitor = available_monitors
            .into_iter()
            .filter(|m| *m != primary)
            .last()
            .unwrap();

        window.set_fullscreen_with(Some(Fullscreen::Borderless(target_monitor)));
    }

    let weather = get_weather();
    let time = Local::now();

    let model = Model { time, weather };

    window.set_visible(true);

    model
}

fn get_weather() -> Result<OpenWeather, String> {
    let config = Config::get()?;
    let url = format!(
        "https://api.openweathermap.org/data/2.5/weather?appid={}&zip={}&units={}",
        config.openweather_api_key, config.zip_code, config.unit_system,
    );

    let res = ureq::get(&url)
        .timeout(Duration::from_secs(5))
        .call();

    if res.error() {
        return Err(res.into_string().map_err(|err| err.to_string())?);
    }

    let weather: Result<OpenWeather, String> = res
        .into_json_deserialize()
        .map_err(|err| err.to_string());

    if let Err(err) = &weather {
        println!("Error retrieving weather: {}", err);
    }
    weather
}

fn on_event(_app: &App, model: &mut Model, event: Event) {
    if let Event::Update(_update) = event {
        model.time = Local::now();
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    draw.background().color(BLACK);

    draw_grid(&draw);

    draw.text(&model.time.format("%H:%M:%S").to_string())
        .x_y(0.0, 150.0)
        .no_line_wrap()
        .font_size(325)
        .justify(nannou::text::Justify::Center)
        .color(WHITE);

    draw.text(&model.time.format("%a, %b %d").to_string())
        .x_y(50.0, -90.0)
        .no_line_wrap()
        .font_size(135)
        .justify(nannou::text::Justify::Right)
        .color(WHITE);

    let weather_text = match &model.weather {
        Ok(weather) => format!("{:.0}°F", weather.main.temp),
        Err(_) => "Err".to_string(),
    };

    draw.text(&weather_text)
        .x_y(425.0, -90.0)
        .no_line_wrap()
        .font_size(135)
        .justify(nannou::text::Justify::Left)
        .color(WHITE);

    draw.to_frame(app, &frame).unwrap();
}

fn draw_grid(draw: &Draw) {
    let grid_color = rgba(1.0, 1.0, 1.0, 0.01);
    let grid_size = 50;
    draw.rect()
        .w_h(1281.0, 721.0)
        .no_fill()
        .stroke_color(grid_color)
        .stroke_weight(1.0);

    for x in (0..641).step_by(grid_size) {
        draw.line()
            .points((x as f32, -360.0).into(), (x as f32, 360.0).into())
            .stroke_weight(1.0)
            .color(grid_color);
        draw.line()
            .points((-x as f32, -360.0).into(), (-x as f32, 360.0).into())
            .stroke_weight(1.0)
            .color(grid_color);
    }
    for y in (0..361).step_by(grid_size) {
        draw.line()
            .points((-640.0, y as f32).into(), (640.0, y as f32).into())
            .stroke_weight(1.0)
            .color(grid_color);
        draw.line()
            .points((-640.0, -y as f32).into(), (640.0, -y as f32).into())
            .stroke_weight(1.0)
            .color(grid_color);
    }
}
