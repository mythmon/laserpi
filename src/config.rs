use serde::Deserialize;
use std::lazy;

static CACHED_CONFIG: lazy::SyncOnceCell<Config> = lazy::SyncOnceCell::new();

#[derive(Deserialize, Debug)]
pub struct Config {
    pub openweather_api_key: String,
    pub zip_code: String,
    pub unit_system: UnitSystem,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum UnitSystem {
    Metric,
    Imperial,
}

impl Config {
    pub fn get() -> Result<&'static Self, String> {
        CACHED_CONFIG.get_or_try_init(|| {
          dotenv::dotenv().map_err(|err| err.to_string())?;
            envy::prefixed("LPI_")
                .from_env::<Config>()
                .map_err(|err| err.to_string())
        })
    }
}

impl std::fmt::Display for UnitSystem {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
      write!(f, "{}", match self {
        Self::Metric => "metric",
        Self::Imperial => "imperial",
      })
    }
}
