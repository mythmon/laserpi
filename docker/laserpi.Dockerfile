FROM ubuntu:20.04

COPY common.sh /
RUN /common.sh

COPY cmake.sh /
RUN /cmake.sh

COPY xargo.sh /
RUN /xargo.sh

ARG DEBIAN_FRONTEND=noninteractive

RUN dpkg --add-architecture armhf
RUN apt-get update
RUN apt-get install --assume-yes apt-utils
RUN apt-get upgrade --assume-yes
RUN apt-get install --assume-yes --no-install-recommends \
  g++-arm-linux-gnueabihf \
  libc6-dev-armhf-cross \
  curl \
  build-essential \
  python \
  python3 \
  cmake \
  pkg-config \
  vulkan-utils \
  libx11-dev \
  libasound2-dev \
  libxcb-shape0-dev \
  libxcb-xfixes0-dev \
  libc6-dev:armhf
RUN apt-get install --assume-yes --no-install-recommends \
  libc6-armhf-cross \
  libc6-dev-armhf-cross

COPY qemu.sh /
RUN /qemu.sh arm softmmu

COPY dropbear.sh /
RUN /dropbear.sh

COPY linux-image.sh /
RUN /linux-image.sh armv7

COPY linux-runner /

ENV CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER=arm-linux-gnueabihf-gcc \
  CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_RUNNER="/linux-runner armv7" \
  CC_armv7_unknown_linux_gnueabihf=arm-linux-gnueabihf-gcc \
  CXX_armv7_unknown_linux_gnueabihf=arm-linux-gnueabihf-g++ \
  QEMU_LD_PREFIX=/usr/arm-linux-gnueabihf \
  RUST_TEST_THREADS=1 \
  PKG_CONFIG_PATH=/usr/lib/arm-linux-gnueabihf/pkgconfig \
  LIBRARY_PATH=/usr/lib/arm-linux-gnueabihf
